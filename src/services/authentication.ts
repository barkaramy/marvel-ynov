import axios from 'axios';

export interface UserData {
    username: string;
    password: string;
}

export const auth = (username: string, password: string): any => {
    const userAuthData: UserData = {
        username: username,
        password: password
    };
    const url = 'https://easy-login-api.herokuapp.com/users/login';
    return axios.post(url, userAuthData);
};
