import axios from 'axios';

export const getManga = (): any => {
    const url = 'https://api.jikan.moe/v3/manga/13/characters';
    return axios
        .post(url)
        .then((response) => {
            console.log(response.headers['x-access-token']);
        })
        .catch((err) => {
            console.log(err);
        });
};
