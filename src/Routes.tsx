import React, { Component } from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import Authentication from './screens/Authentication';
import Characters from './screens/Characters';
import history from './utils/history';

const Routes = (): JSX.Element => {
    return (
        <Router history={history}>
            <Switch>
                <Route exact path='/' component={Authentication} />
                <Route path='/characters' exact component={Characters} />
            </Switch>
        </Router>
    );
};

export default Routes;
