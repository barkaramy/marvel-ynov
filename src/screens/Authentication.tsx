import React, { useState } from 'react';
import styled from 'styled-components';
import { auth } from '../services/authentication';
import Input from '../components/input/Input';

const Authentication = (props): JSX.Element => {
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const login = (e): void => {
        e.preventDefault();
        auth(userName, password)
            .then((response) => {
                localStorage.setItem('token', response.headers['x-access-token']);
                props.history.push('/characters');
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <AuthenticationContainer>
            <FormContainer onSubmit={login}>
                {/* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */}
                <Input icon={null} label='username' inputType='text' onChange={(e) => setUserName(e.target.value)} />
                <Input icon={null} label='password' inputType='text' onChange={(e) => setPassword(e.target.value)} />
                <Input icon={null} label={null} inputType='submit' />
            </FormContainer>
        </AuthenticationContainer>
    );
};

const AuthenticationContainer = styled.div`
    height: 100%;
    align-items: center;
    display: flex;
    justify-content: center;
`;

const FormContainer = styled.form`
    width: 50%;
`;

export default Authentication;
