import React from 'react';
import styled from 'styled-components';

interface Props {
    label: string | null;
    inputType: string;
    icon: React.ReactNode;
    onChange?: (e) => void;
}

const Input = (props: Props): JSX.Element => {
    const { label, icon, inputType, onChange } = props;
    return (
        <InputContainer>
            {label && <Label>{label}</Label>}
            <DivInput>
                <Inputtag type={inputType} onChange={onChange} />
                {icon}
            </DivInput>
        </InputContainer>
    );
};

const InputContainer = styled.div``;

const Label = styled.label`
    color: red;
`;

const DivInput = styled.div``;

const Inputtag = styled.input`
    border-radius: 20px;
`;

export default Input;
